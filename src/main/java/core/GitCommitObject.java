package core;

import java.io.File;

public class GitCommitObject extends GitObject{

	public GitCommitObject(File repoPath, String masterCommitHash) {
		super(repoPath, masterCommitHash);
	}

	public String getParentHash() {
		return executeInShell("git rev-parse " + this.hash + "^").trim();
	}

	public String getAuthor() {	
		return executeInShell("git --no-pager show -s --format='%an <%ae>' " + this.hash).trim();
	}
}
