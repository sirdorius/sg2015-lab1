package core;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GitRepository {
	private File path;
	
	public GitRepository(File path) {
		this.path = path;
	}
	
	public String getHeadRef() {
		String f = FSHelper.readFile(path.getAbsolutePath() + "/HEAD");
		
		Pattern r = Pattern.compile("ref: (.*)\n");
		Matcher m = r.matcher(f);
		if (m.find()) {
			return m.group(1);
		}
		return "HEADREF error";
	}
	
	public String getRefHash(String refPath) {
		try {
			File file = new File(this.path.getAbsolutePath() + "/" + refPath);
			BufferedReader br = new BufferedReader(new FileReader(file));
			String s = br.readLine();
			br.close();
			return s;
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		return "Error";
	}
}