package core;

import java.io.File;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class GitTreeObject extends GitObject {

	public GitTreeObject(File repoPath, String masterTreeHash) {
		super(repoPath, masterTreeHash);
	}

	public GitObject getEntry(String entryName) {
		String entryLine = executeInShell("git cat-file -p " + this.hash + "^{tree} | grep " + entryName);
		String type = entryLine.split(" ")[1];
		String entryHash = entryLine.split(" ")[2].split("\t")[0];
		switch (type) {
			case "blob":
				return new GitBlobObject(repoPath, entryHash);
			case "tree":
				return new GitTreeObject(repoPath, entryHash);
			// TODO add commit type
		}
		return null;
	}

	public Collection<String> getEntryPaths() {
		String s = executeInShell("git cat-file -p " + this.hash + "^{tree}").trim();
		List<String> objectList = Arrays.asList(s.split("\n"));
		for (int i=0; i<objectList.size(); i++) {
			String item = objectList.get(i);
			objectList.set(i, item.split("\t")[1]);
		}
		return objectList;
	}

}
