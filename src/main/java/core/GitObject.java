package core;

import java.io.File;

public abstract class GitObject {

	protected File repoPath;
	protected String hash;
	
	public GitObject(File repoPath, String hash) {
		this.repoPath = repoPath;
		this.hash = hash;
	}
	
	public String getType() {
		return executeInShell("git cat-file -t " + this.hash).trim();
	}

	public String getHash() {
		return this.hash;
	}

	public String getTreeHash() {
		return executeInShell("git rev-parse " + this.hash + "^{tree}").trim();
	}
	
	String executeInShell(String cmd) {
		return FSHelper.executeInShell(cmd, this.repoPath);
	}
}
