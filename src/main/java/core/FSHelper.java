package core;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class FSHelper {
	static String executeInShell(String cmd, File path) {
		String out = "";
		String[] cmdWrapper = {"/bin/sh", "-c", "cd "+ path + "; " + cmd};
		
		try {
			Process p = Runtime.getRuntime().exec(cmdWrapper);
			p.waitFor();
			
			BufferedReader b = new BufferedReader(new InputStreamReader(p.getInputStream()));
			BufferedReader err = new BufferedReader(new InputStreamReader(p.getErrorStream()));
			
			String line;
			while ((line = err.readLine()) != null)
				out += line + "\n";
			if (!out.trim().equals("")) {
				System.out.println("Executing command in shell failed:\n" + cmd + "\n" + out);
			}
			while ((line = b.readLine()) != null)
				out += line + "\n";
			
			return out;
		}
		catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
		return "ERROR";
	}
	
	static String readFile(String path) {
		String content = "ERROR reading file at " + path;
		try {
			File headFile = new File(path);
			char[] buffer = new char[(int)headFile.length()];
			FileReader reader = new FileReader(headFile);
			reader.read(buffer);
			reader.close();
			content = new String(buffer);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return content;
	}
}
