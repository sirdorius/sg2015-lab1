package core;

import java.io.File;

public class GitBlobObject extends GitObject {
	public GitBlobObject(File repoPath, String hash) {
		super(repoPath, hash);
	}

	public String getContent() {
		return executeInShell("git cat-file -p " + this.hash);
	}
}
