package core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.Arrays;
import org.junit.Test;


public class ReadGitRepositoryTest {

	private final File repoPath = new File("sample_repos/sample01");
	private final GitRepository repository = new GitRepository(repoPath);
	
	//trovare da shell l'hash del commit relativo al master del repository in sample_repos/sample01
	private static String masterCommitHash = "1e7c193371b42215bc9ba653c48f58ebbb1a7aae"; 

	//trovare da shell l'hash del tree relativo al master del repository in sample_repos/sample01
	private static String masterTreeHash = "b97a44efe72ac12363fd19654c348e81e22a0266";

	@Test
	public void shouldFindHead() throws Exception {
		assertEquals("refs/heads/master",repository.getHeadRef());
	}

	@Test
	public void shouldFindHash() throws Exception {
		assertEquals(masterCommitHash,repository.getRefHash("refs/heads/master"));
	}

	@Test
	public void shouldGetBlobContent() throws Exception {
		GitBlobObject blobObject = new GitBlobObject(repoPath,"4452771b4a695592a82313e3253f5e073e6ead8c");
		assertEquals("blob",blobObject.getType());
		assertEquals("REPO DI PROVA\n=============\n\nSemplice repository Git di prova\n", blobObject.getContent());		
	}

	@Test
	public void shouldGetCommitObject() throws Exception {
		GitCommitObject commit = new GitCommitObject(repoPath, masterCommitHash);
		assertEquals(masterCommitHash,commit.getHash());
		assertEquals(masterTreeHash,commit.getTreeHash());
		assertEquals("c4c62d173aa9ebd5497f10db6b9826539e60108e",commit.getParentHash());
		assertEquals("Carlo Bellettini <carlo.bellettini@unimi.it>",commit.getAuthor());
	}
	
	@Test
	public void shouldGetTreeObject() throws Exception {
		GitTreeObject treeObject = new GitTreeObject(repoPath, masterTreeHash);
		assertTrue(Arrays.asList("file4","file2", "dir1", "file1","README.md").containsAll(treeObject.getEntryPaths()));
		assertTrue(treeObject.getEntry("README.md") instanceof GitBlobObject);
		assertEquals("tree", treeObject.getEntry("dir1").getType());
		assertEquals("4452771b4a695592a82313e3253f5e073e6ead8c", treeObject.getEntry("README.md").getHash());
	}
}
